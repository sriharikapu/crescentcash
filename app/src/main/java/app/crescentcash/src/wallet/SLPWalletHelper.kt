package app.crescentcash.src.wallet

import com.bitcoin.slpwallet.SLPWalletConfig

class SLPWalletHelper {
    companion object {
        fun setAPIKey() {
            SLPWalletConfig.restAPIKey = "slpsdkH6aIcXEApC4wXQfqqPH"
        }

        fun blockieAddressFromTokenId(tokenId: String): String {
            return tokenId.slice(IntRange(12, tokenId.count() - 1))
        }
    }

}