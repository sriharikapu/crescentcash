package app.crescentcash.src.json

import org.json.JSONException
import org.json.JSONObject

import java.io.IOException
import java.io.Reader

class JSONHelper {

    fun getRegisterTxHash(jsonResponse: String): String {
        var hash = ""
        try {
            val json = JSONObject(jsonResponse)
            hash = json.getString("txid")
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        return hash
    }

    companion object {

        @Throws(IOException::class)
        fun readJSONFile(rd: Reader): String {
            val sb = StringBuilder()
            while (true) {
                val cp = rd.read()

                if(cp != -1)
                    sb.append(cp.toChar())
                else
                    break
            }
            return sb.toString()
        }
    }
}
